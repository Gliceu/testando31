
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Pratica31A {

    public static void main(String[] args) {
        Date inicio = new Date();
           Calendar nasc = new GregorianCalendar(1965, 02, 02);
           Calendar hoje = new GregorianCalendar();
              String meuNome = "Gliceu Fernandes de Camargo";
              System.out.println("Meu nome é: " + meuNome.toUpperCase());
              System.out.println(meuNome.toUpperCase().charAt(20) + "" + meuNome.substring(21, 27).toLowerCase() + ", " 
                + meuNome.toUpperCase().charAt(0) + "."+ meuNome.toUpperCase().charAt(7) + ".");
              
        System.out.println("Até a data de hoje foram " + dias(hoje.getTime(), nasc.getTime()) + " dias decorridos ");
        Date fim = new Date();
        long tempoExecucao = fim.getTime() - inicio.getTime();
        System.out.println("A execução destes programa foi de " + tempoExecucao + " milisegundos");

    }

    static int dias(java.util.Date hoje, java.util.Date nasc) {
        return (int) ((hoje.getTime() - nasc.getTime()) / (1000 * 60 * 60 * 24));
    }

}

